import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import { LoadUsersFailure, LoadUsersSuccess, AdminActionTypes, AdminActions } from './admin.actions';
import { UserManagerService } from './services/user-manager.service';




@Injectable()
export class AdminEffects {

  @Effect()
  loadAdmins$ = this.actions$.pipe(
    ofType(AdminActionTypes.LoadUsers),
    concatMap(() =>
      this.userService.getUsers().pipe(
        map(data => new LoadUsersSuccess({ data })),
        catchError(error => of(new LoadUsersFailure({ error }))))
    )
  );



  constructor(
    private actions$: Actions<AdminActions>,
    private userService: UserManagerService
  ) { }

}
