
import { AdminActions, AdminActionTypes } from './admin.actions';
import { User } from '../user/models';

export const adminFeatureKey = 'admin';

export interface State {
  users: User[];
  usersError: any;
  selected: User;
}

export const initialState: State = {
  users: [],
  usersError: null,
  selected: null
};

export function reducer(state = initialState, action: AdminActions): State {
  switch (action.type) {

    case AdminActionTypes.LoadUsers:
      return state;

    case AdminActionTypes.LoadUsersSuccess:
      return {
        ...state,
        users: action.payload.data
      };

    case AdminActionTypes.LoadUsersFailure:
      return state;

    default:
      return state;
  }
}
