import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { AdminUsersPageComponent } from './pages/admin-users-page/admin-users-page.component';
import { UserDetailsPageComponent } from './pages/user-details-page/user-details-page.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { StoreModule } from '@ngrx/store';
import * as fromAdmin from './admin.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AdminEffects } from './admin.effects';


@NgModule({
  declarations: [AdminPageComponent, AdminUsersPageComponent, UserDetailsPageComponent, UserFormComponent, UserDetailsComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    StoreModule.forFeature(fromAdmin.adminFeatureKey, fromAdmin.reducer),
    EffectsModule.forFeature([AdminEffects])
  ],
  providers: []
})
export class AdminModule { }
