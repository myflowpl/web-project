import { Action } from '@ngrx/store';
import { User } from '../user/models';

export enum AdminActionTypes {
  LoadUsers = '[Admin] Load Users',
  LoadUsersSuccess = '[Admin] Load Users Success',
  LoadUsersFailure = '[Admin] Load Users Failure',
}

export class LoadUsers implements Action {
  readonly type = AdminActionTypes.LoadUsers;
}

export class LoadUsersSuccess implements Action {
  readonly type = AdminActionTypes.LoadUsersSuccess;
  constructor(public payload: { data: User[] }) { }
}

export class LoadUsersFailure implements Action {
  readonly type = AdminActionTypes.LoadUsersFailure;
  constructor(public payload: { error: any }) { }
}

export type AdminActions = LoadUsers | LoadUsersSuccess | LoadUsersFailure;

