import { Injectable, Inject } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { delay, map, filter, debounceTime, switchMap } from 'rxjs/operators';
import { User } from '../models';
import { HttpClient } from '@angular/common/http';
import { API_BASE_PATH } from '../../config/tokens';

@Injectable({
  providedIn: 'root'
})
export class UserManagerService {

  constructor(
    private http: HttpClient,
    @Inject(API_BASE_PATH) private basePath: string,
  ) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.basePath + '/users');
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(this.basePath + '/users/' + id);
  }

  updateUser(user: User): Observable<User> {
    return this.http.patch<User>(this.basePath + '/users/' + user.id, user);
  }
}
