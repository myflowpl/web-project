import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAdmin from './admin.reducer';

export const selectAdminState = createFeatureSelector<fromAdmin.State>(
  fromAdmin.adminFeatureKey
);

export const selectAdminUsers = createSelector(
  selectAdminState,
  (admin) => admin.users
);

export const selectAdminUserById = (id) => {
  return createSelector(
    selectAdminUsers,
    (users) => users.find(user => user.id === parseInt(id, 10))
  );
};
