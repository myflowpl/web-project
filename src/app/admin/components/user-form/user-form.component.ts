import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { User } from '../../../user/models';
import { FormBuilder, Validators } from '@angular/forms';
import { UserManagerService } from '../../services/user-manager.service';
import { UserManagerFacade } from '../../facades/user-manager.facade';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit, OnChanges {

  @Input()
  user: User;

  @Output()
  saved = new EventEmitter();

  @Output()
  cancelled = new EventEmitter();

  userForm = this.fb.group({
    id: [],
    email: ['', [Validators.email, Validators.required]],
    password: ['', [Validators.required]],
    name: ['', [Validators.required]],
  });
  constructor(
    private fb: FormBuilder,
    private userFacade: UserManagerFacade
  ) { }

  ngOnInit() {
    console.log('INIT FORM');
  }

  ngOnChanges(changes) {
    console.log('CHANGES', changes);
    if (changes.user) {
      this.userForm.patchValue(changes.user.currentValue || {});
    }
  }

  onSubmit() {
    this.userFacade.updateUser(this.userForm.value).subscribe(
      user => this.saved.emit(user)
    );
  }
}
