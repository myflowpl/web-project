import { Injectable } from '@angular/core';
import { User } from '../../user/models';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { UserManagerService } from '../services/user-manager.service';
import { Store } from '@ngrx/store';
import { State } from '../admin.reducer';
import { LoadUsers } from '../admin.actions';
import { selectAdminUsers, selectAdminUserById } from '../admin.selectors';

@Injectable({
  providedIn: 'root'
})
export class UserManagerFacade {

  private _users$ = new BehaviorSubject<User[]>([]);

  users$ = this.store.select(selectAdminUsers);

  constructor(
    private userService: UserManagerService,
    private store: Store<State>
  ) { }

  loadUsers() {
    this.store.dispatch(new LoadUsers());
  }

  userById$(id): Observable<User> {
    return this.store.select(selectAdminUserById(id));
  }

  updateUser(newUser: User) {
    return this.userService.updateUser(newUser).pipe(
      tap(userUpdated => {
        const users = this._users$.getValue().map(user => (user.id === userUpdated.id) ? userUpdated : user);
        this._users$.next(users);
      })
    );
  }
}
