import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { AdminUsersPageComponent } from './pages/admin-users-page/admin-users-page.component';
import { UserDetailsPageComponent } from './pages/user-details-page/user-details-page.component';
import { AuthGuard } from '../user/guards/auth.guard';


const routes: Routes = [{
  path: '',
  component: AdminPageComponent,
  canActivate: [AuthGuard],
  data: {
    roles: ['admin']
  },
  children: [{
    path: '',
    redirectTo: 'users',
    pathMatch: 'full'
  }, {
    path: 'users',
    component: AdminUsersPageComponent,
    children: [{
      path: 'details/:id',
      component: UserDetailsPageComponent
    }]
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
