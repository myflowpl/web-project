import { Component, OnInit, OnDestroy, ViewChild, ComponentRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserManagerService } from '../../services/user-manager.service';
import { User } from '../../models';
import { switchMap, catchError, takeUntil, tap, startWith } from 'rxjs/operators';
import { EMPTY, Subscription, Subject, Observable } from 'rxjs';
import { SubSink } from '../../../config/sub-sink';
import { UserFormComponent } from '../../components/user-form/user-form.component';
import { UserManagerFacade } from '../../facades/user-manager.facade';

@Component({
  selector: 'app-user-details-page',
  templateUrl: './user-details-page.component.html',
  styleUrls: ['./user-details-page.component.scss']
})
export class UserDetailsPageComponent implements OnInit {

  // @ViewChild('form', {static: false})
  form: UserFormComponent;

  user$: Observable<User>;
  error: any;
  isEdit = true;
  constructor(
    private userFacade: UserManagerFacade,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    console.log('INIT DETAILS', this.form);

    this.user$ = this.route.params.pipe(
      switchMap(params => this.userFacade.userById$(params.id).pipe(
        // startWith(null),
        tap({
          next: user => this.error = null,
          error: err => this.error = err
        }),
        catchError(err => EMPTY),
      ))
    );
  }

  onFormSave(user) {
    this.isEdit = false;
    console.log('SAVE USER', user);
  }

}
