import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { User } from '../../models';
import { UserManagerService } from '../../services/user-manager.service';
import { UserManagerFacade } from '../../facades/user-manager.facade';

@Component({
  selector: 'app-admin-users-page',
  templateUrl: './admin-users-page.component.html',
  styleUrls: ['./admin-users-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class AdminUsersPageComponent implements OnInit {

  @Input()
  buttonText = 'Add New';

  users$ = this.userFacade.users$;

  constructor(
    private userFacade: UserManagerFacade
  ) { }

  ngOnInit() {
    this.userFacade.loadUsers();
  }

  onNewClick(e) {
    console.log('NEW CLICK', e);
  }
}
