import { Subscription } from 'rxjs';

export class SubSink {

  subs: Subscription[] = [];

  set add(sub) {
    this.subs.push(sub);
  }

  unsubscribe() {
    this.subs.forEach(sub => sub.unsubscribe());
  }
}
