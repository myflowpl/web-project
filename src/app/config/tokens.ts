import { InjectionToken } from '@angular/core';

export const API_BASE_PATH = new InjectionToken('Api Base Path');
