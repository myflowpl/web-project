import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    FormsModule,
    MatInputModule,
    RouterModule
  ],
  exports: [
    FormsModule,
    MatInputModule,
    RouterModule
  ]
})
export class CoreModule {

}
