import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  @Output()
  loginSuccess = new EventEmitter();

  @Output()
  loginCancel = new EventEmitter();

  error: any;
  loader = new Loader();

  loginForm = this.fb.group({
    username: ['piotr@myflow.pl', [Validators.email, Validators.required]],
    password: ['123', [Validators.required]]
  });

  constructor(
    private fb: FormBuilder,
    private userService: UserService
  ) { }

  ngOnInit() {

  }
  onSubmit() {
    this.error = null;
    this.userService.login(this.loginForm.value).pipe(
      takeUntil(this.loader.working$)
    ).subscribe(
      res => this.loginSuccess.emit(res),
      err => this.error = err.error
    );
  }
}

export class Loader {
  active = false;
  working$ = new Observable(obs => {
    this.active = true;
    return () => this.active = false;
  });
}

