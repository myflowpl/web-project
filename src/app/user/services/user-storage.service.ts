import { Injectable } from '@angular/core';
import { TokenPayload } from '../models/user-models';

@Injectable({
  providedIn: 'root'
})
export class UserStorageService {

  PAYLOAD_KEY = 'token-payload';

  setPayload(payload: TokenPayload) {
    localStorage.setItem(this.PAYLOAD_KEY, JSON.stringify(payload));
  }
  getPayload(): TokenPayload | null {
    const payloadStr = localStorage.getItem(this.PAYLOAD_KEY);
    if (payloadStr) {
      return JSON.parse(payloadStr);
    }
    return null;
  }
}
