import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_BASE_PATH } from '../../config/tokens';
import { tap, map, debounceTime } from 'rxjs/operators';
import { BehaviorSubject, Observable, Observer } from 'rxjs';
import { TokenPayload, User, Profile } from '../models';
import { UserStorageService } from './user-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  payload$ = new BehaviorSubject<TokenPayload | null>(this.storage.getPayload());

  user$ = this.payload$.pipe(
    map((payload: TokenPayload) => (payload && payload.user) ? payload.user : null)
  );

  get accessToken() {
    const payload = this.payload$.getValue();
    return (payload) ? payload.access_token : '';
  }

  constructor(
    private storage: UserStorageService,
    private http: HttpClient,
    @Inject(API_BASE_PATH) private basePath: string
  ) {
    this.payload$.subscribe(payload => this.storage.setPayload(payload));
  }


  login(credentials: { username: string, password: string }) {
    return this.http.post<TokenPayload>(this.basePath + '/auth/login', credentials).pipe(
      tap(payload => this.payload$.next(payload))
    );
  }

  logout() {
    return new Observable(obs => {
      this.payload$.next(null);
      obs.next(true);
      obs.complete();
    });
  }

  getProfile() {
    return this.http.get<Profile>(this.basePath + '/auth/profile');
  }
}
