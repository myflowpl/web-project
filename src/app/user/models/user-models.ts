import { User } from './user-model';

export interface TokenPayload {
  access_token: string;
  user: User;
}
export interface Comment {
  id: number;
  text: string;
}
export interface Profile {
  user: User;
  comments: Comment[];
}
