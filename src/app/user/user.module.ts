import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';
import { CoreModule } from '../core.module';

@NgModule({
  declarations: [LoginPageComponent, LoginFormComponent, ProfilePageComponent],
  imports: [
    CoreModule,
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class UserModule { }
