import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Observable } from 'rxjs';
import { Profile } from '../../models/user-models';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  profile$: Observable<Profile>;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.profile$ = this.userService.getProfile();
  }

}
